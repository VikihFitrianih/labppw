from django.conf.urls import url
from . import views
from django.shortcuts import render
from django.urls import path

app_name = 'Schedule'
urlpatterns = [
    path('sched_add/', views.sched_add, name='sched_add'),
    path('sched_add_save/<int:id>', views.sched_add_save, name='sched_add_save'),
    path('sched_edit/<int:id>', views.sched_edit, name='sched_edit'),
    path('sched_view/', views.sched_view, name='sched_view_all'),
    path('sched_view/<int:id>', views.sched_display, name='sched_view'),
    path('sched_delete/<int:pk>', views.sched_delete, name='sched_delete'),
]
