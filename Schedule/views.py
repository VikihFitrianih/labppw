from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from .forms import ScheduleForm
from .models import Schedule
from django.shortcuts import redirect
import datetime


# Create your views here.
def sched_add(request):
    if request.method == "POST":
        form = ScheduleForm(request.POST)
        print(form.is_valid())
        print(form.errors)
        if form.is_valid():
            response = {}
            response['title'] = request.POST['title']
            response['day'] = request.POST['day']
            response['date'] = request.POST['date']
            response['time'] = request.POST['time']
            response['location'] = request.POST['location']
            response['category'] = request.POST['category']
            response['description'] = request.POST['description']
            sched_item = Schedule(title=response['title'], day=response['day'],date=response['date'],time=response['time'],location=response['location'],category=response['category'],description=response['description'])
            sched_item.save()
            return redirect(reverse('Schedule:sched_add'))
    else:
        form = ScheduleForm()
    return render(request, 'ScheduleForm.html', {'title':'Schedule Form','form': form})


def sched_add_save(request, id=None):
    item = Schedule.objects.get(id=id)
    if request.method == "POST":
        form = ScheduleForm(request.POST, instance=item) 
        if form.is_valid():
            form.save()
            return redirect(reverse('Schedule:sched_add_save'))
    else:
        form = ScheduleForm(instance=post)
        return redirect(reverse('Schedule:sched_view'))
    #return render(request, 'Schedule/jadwal_form.hrml', {'form': form})


def sched_edit(request, id=id):
    item = Schedule.objects.get(id=id)
    form = ScheduleForm(instance=item)
    return render(request, 'sched_edit.html', {'title':'Schedule Form','form':form,'item':item})


def sched_view(request):
    data = Schedule.objects.all()
    return render(request, 'sched_view.html', {'data': data})


def sched_display(request, id=id):
    item = Schedule.objects.get(id=id)
    return render(request, 'sched_display.html', {'item':item})


def sched_delete(request, pk):
    Schedule.objects.filter(pk = pk).delete()
    db_item = Schedule.objects.all()
    content = {'title':'Schedule', 'db_item':db_item}
    return redirect('/Schedule/sched_view/')
